The security model of Android sets up two levels of app installation/uninstallation processes: privileged and non-privileged.

### Privileged

Only app stores that come built into the device can use the privileged APIs and processes, which expect the privileged app to handle all prompting of the user.  That means that Android itself does no prompting when using the privileged process.  These privileges are automatically enabled from the first start of the device.  These APIs and processes allow for a tightly integrated UX, where the app store controls the entire UX and user prompting.  Built-in app stores like F-Droid (with [Privileged Extension](https://gitlab.com/fdroid/privileged-extension/)), Google Play, Huawei App Gallery, etc. use this approach.

### Non-privileged

Apps that want to install/update/uninstall other apps have to use the non-privileged APIs and processes.  When using this process, the Android OS itself decides when the user should be prompted, and when not.  The app running the install/update/uninstall has no control over the prompting.  In order for this app to get access to these APIs and processes, the user must first go through the system's Unknown Sources process, and enable it for that app.  After that, the app can submit install/update/uninstall requests.   All installs and uninstalls require the user confirm a system-provided prompt.  Updates that meet that Android version's criteria can be achieved without additional prompts.  All other updates will require the user to confirm a system-provided prompt.


# F-Droid Privileged Extension

[F-Droid Privileged Extension](https://gitlab.com/fdroid/privileged-extension/) is a tiny shim app designed to be built into the Android OS to give _fdroidclient_ access to the privileged APIs and processes.  It is designed to be as small as possible, so it can be easily understood, audited, and maintained.  It is designed to have zero access to any network, e.g fully offline.  This also means it has the smallest attack surface for a privileged setup.  This is in contrast with Google Play Services, which is what Google Play uses for its privileged service.  It is a giant, network-enabled, proprietary bag of functionality running with elevated privileges.

# First-time installs as exploit vector

The Android OS will ensure that updates match the signer of the installed app, regardless of how it is installed.  No matter which method is used for installing an app, anything that can install an app for the first time has to be trusted because the installer can easily tell the user that it is installing the expected app while downloading and installing a malicious version.  This exploit vector exists even when using the most restrictive install API, (e.g. an unprivileged call to the session installer).

F-Droid Privileged Extension allows F-Droid to transparently issue background install and uninstall requests.  It is conceivable that a bug could exist in F-Droid that could be exploited to issue background  requests to do malicious things.  We put a big effort into secure code and security audits to ensure this is not possible.  While this exploit vector exists, it is also important to note: anything that can install an app for the first time will present a very similar exploit vector.  Also, this still only applies to first-time installs.
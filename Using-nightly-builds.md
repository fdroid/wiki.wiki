
Get a sneak peek at our current work, and help us make it the best it can be: try F-Droid Nightly.  There are "nightly" builds of the standard F-Droid Android client app, as well as a test version of F-Droid "Basic". We always appreciate testing, feedback, issues, and crash reports.  Be aware: the nightly builds built, signed, and released by GitLab CI/CD, so not as secure a release process as the official releases.

A spare device, test device, and Android emulators are the ideal place to play with nightly builds. Nightly builds are debug builds, meaning there are many troubleshooting features enabled that are disabled in the release builds.  This means that private information could leak to your device's log, and perhaps elsewhere.

## Installing

The easiest way to get started with the nightly builds is to add the [repo](https://gitlab.com/fdroid/fdroidclient-nightly/-/raw/master/fdroid/repo) to an existing F-Droid install, then search for "Nightly", then choose "F-Droid Debug". It is possible to have both official and nightly installed at the same time.

* To download and install the APK directly, find the newest version of the APK you want in this listing: <https://gitlab.com/fdroid/fdroidclient-nightly/-/tree/master/fdroid/repo>

* To install them on a device that already has F-Droid on it, you can scan or click this QR Code to add a new repo to get the nightly builds:

[![F-Droid Nightly Repo](uploads/aed2341dd5e5819369ec46e2c7e58fc6/qr.png)](https://gitlab.com/fdroid/fdroidclient-nightly/-/raw/master/fdroid/repo)

To learn about the whole nightly setup, there is documentation all about [publishing nightly builds](https://f-droid.org/docs/Publishing_Nightly_Builds/) with `fdroid nightly`.

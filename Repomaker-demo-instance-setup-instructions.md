---
title: Repomaker  instance setup
---

Currenlty, Repomaker demo instance can be accessed on  [repomakerdemo.fdroid.net](https://repomakerdemo.fdroid.net) . This instance is running on  a Debian 12 OSUSOSL VM. This server also has a gitlab runner which assists with Repomaker CI/CD.

This document aims to be an easy guide on how to get Repomaker up and running as fast as possible on any Debian based distro.
1. Install Docker. [Here](https://docs.docker.com/engine/install/debian/) is a guide on how to install it on Debian. Do not forget to follow the post install instructions as described [here](https://docs.docker.com/engine/install/linux-postinstall/).
2. Install Apache webserver and make sure it is running. From the cli this can be checked with the command : `sudo apt install apache2 && sudo systemctl status apache2`
3. On your domain registrar, link your domain to the server IP intended to host Repomaker on.
4. Create a conf file on  `/etc/apache2/sites-available/` titled the domain name you intend to serve Repomaker from. For our case it will be `repomakerdemo.fdroid.net.conf`
5. Edit `/etc/apache2/sites-available/repomakerdemo.fdroid.net.conf` to serve the index page proxied on port 84. The content should be as follows:
  ```
  <VirtualHost *:80>
    ServerName repomakerdemo.fdroid.net

    ProxyPreserveHost On
    ProxyPass / http://0.0.0.0:84/
    ProxyPassReverse / http://0.0.0.0:84/

    ErrorLog ${APACHE_LOG_DIR}/yourapp_error.log
    CustomLog ${APACHE_LOG_DIR}/yourapp_access.log combined
  </VirtualHost>
  <VirtualHost *:443> 
        ServerName repomakerdemo.fdroid.net

        ProxyPreserveHost On
        ProxyPass / http://0.0.0.0:84/
        ProxyPassReverse / http://0.0.0.0:84/

</VirtualHost>
```
 Close the file and run `/usr/sbin/apache2ctl configtest` to test any changes.

6. Enable the site and enable the modules with the command `sudo a2ensite repomakerdemo.fdroid.net.conf && sudo a2enmod proxy proxy_http`  
7. Clone Repomaker from gitlab with the command `https://gitlab.com/fdroid/repomaker` and move into the docker folder with the command`cd docker`   
8. Edit the `.env` file by adding the `REPOMAKER_PORT=84` on the `.env` and adding the domain name on the variable `REPOMAKER_HOSTNAME=repomakerdemo.fdroid.net`
9. To run Repomaker, run the command `docker compose pull && docker compose up -d`. The repomaker images will be fetched from Gitlab repository.Once done you can access your Repomaker instance on your domain name, for this case it will be on `repomakerdmo.fdroid.net`
10. Access your domain on a browser tab. You should see Repomaker index page but on HTTP.
11. To enable HTTPS access, use [certbot](https://certbot.eff.org/) to automatically create certificates. First, install the packages on the cli by running `sudo apt install python3-venv libaugeas0`  
12. Remove Certbot and any pre-existing cerbot packages by running `sudo apt-get remove certbot`   
13. Set up a python virtual environment by running `sudo python3 -m venv /opt/certbot/ && sudo /opt/certbot/bin/pip install --upgrade pip` on the terminal    
14. Install Certbot by running `sudo /opt/certbot/bin/pip install certbot certbot-apache` on the terminal
15. Prepare Cerbot by running `sudo ln -s /opt/certbot/bin/certbot /usr/bin/certbot` on the terminal 
16. Obtain and install the HTTPS certificate on Apache by running `sudo certbot --apache`. This will allow Certbot edit your apache configuration automatically to serve it, turning on HTTPS access in a single step.
17. Reload the apache service by running `sudo systemctl reload apache2` on the terminal
18. Set up automatic renewal of Certbot by running the command `echo "0 0,12 * * * root /opt/certbot/bin/python -c 'import random; import time; time.sleep(random.random() * 3600)' && sudo certbot renew -q" | sudo tee -a /etc/crontab > /dev/null` on the terminal
19.  Open your domain on the webbrowser to see your repomaker being served on HTTPS

## References
- [Docker Set up](https://docs.docker.com/engine/install/debian/)
- [Docker post installation](https://docs.docker.com/engine/install/linux-postinstall/)
- [Apache Webserver](https://httpd.apache.org/)
- [Certbot](https://certbot.eff.org/instructions?ws=apache&os=pip)


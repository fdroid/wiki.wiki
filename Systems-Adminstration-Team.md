Notes on sysadmining the F-Droid infrastructure.

(_If we want a three letter acronym, it can be FSA :wink:_)

## Dedicated Laptop

All core systems should only be accessed by very limited machines that are set up only for terminal-based sysadmin activities.

### General rules of operation

* The secure machine should only be used for sysadmining machines.
* The secure machine should only have the required software for the task, which is mostly a terminal and SSH.
* The SSH/etc keys used on the secure machine should not be used on other machines, especially a regular laptop used for browsing, messaging, etc.
* The keys should only be used via a smartcard like a Nitrokey.
* All remote access methods should be removed from the system.
* There should absolutely no open network ports on the system.
* If a browser is installed, it should default to disallowing all JavaScript for all sites.
* Bluetooth should be entirely disabled (e.g. blacklist the Linux kernel module.)
* Ethernet is preferred over Wi-Fi.
* Network adapters should use all free software drivers, and not have a cellular baseband built-in (e.g. LTE, 5G, etc).


### Recommended setups

* Very minimal Debian install with stripped down configuration.
* TAILS
* Qubes


## Interesting supporting materials 

* [matrix.org: Post-mortem and remediations for Apr 11 security incident](https://matrix.org/blog/2019/05/08/post-mortem-and-remediations-for-apr-11-security-incident/)
  * A detailed post mortem based on the materials that the hacker who broke in posted online.
* How an exploit in a webpage can lead to total pwnage (browser -> BIOS pwnage -> total pwnage)
  * https://arstechnica.com/security/2023/12/just-about-every-windows-and-linux-device-vulnerable-to-new-logofail-firmware-attack/2/
* [Enhanced Security Requirements for Protecting Controlled Unclassified Information: A Supplement to NIST Special Publication 800-171](https://csrc.nist.gov/pubs/sp/800/172/final)
  * How the US Gov thinks about this, there is a lot of jargon and boilerplate text to skip through, but it is the product of a lot of experience and thinking.
  * Some key points:
    * Employ system and security architectures that support logical and physical isolation using system and network segmentation techniques, virtual machines, and containers
    * Implement dual authorization controls for the most critical or sensitive operations
    * Limit persistent storage to isolated enclaves or domains
    * Use deception to confuse and mislead adversaries regarding the information they use for
decision-making, the value and authenticity of the information they attempt to exfiltrate, or the environment in which they are operating.
    * Establish and maintain an authoritative source and repository to provide a trusted source and accountability for approved and implemented system components
    * Employ automated mechanisms to detect misconfigured or unauthorized system components
* [Developing Cyber Resilient Systems: A Systems Security Engineering Approach](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-160v2.pdf)
  * Some key points:
    * Analytic Monitoring: Monitor and analyze a wide range of properties and behaviors on an ongoing basis and in a coordinated way;
    * Coordinated Protection: Ensure that protection mechanisms operate in a coordinated and effective manner;
    * Deception: Mislead, confuse, hide critical assets from, or expose covertly tainted assets to the adversary;
    * Diversity: Use heterogeneity to minimize common mode failures, particularly threat events exploiting common vulnerabilities;
    * Dynamic Positioning: Distribute and dynamically relocate functionality or system resources;
    * Non-Persistence: Generate and retain resources as needed or for a limited time;
    * Privilege Restriction: Restrict privileges based on attributes of users and system elements as well as on environmental factors;
    * Redundancy: Provide multiple protected instances of critical resources;
    * Segmentation: Define and separate system elements based on criticality and trustworthiness;
    * Substantiated Integrity: Ascertain whether critical system elements have been corrupted; and
    * Unpredictability: Make changes randomly or unpredictably.
* [Since I Met Edward Snowden, I’ve Never Stopped Watching My Back](https://www.theatlantic.com/magazine/archive/2020/06/edward-snowden-operation-firstfruits/610573/)
  * > On bathroom breaks, [Snowden] took his laptop with him. “There’s a level of paranoia where you go, ‘You know what? This could be too much,’ ” he said when I smiled at this. “But it costs nothing. It’s—you get used to it. You adjust your behavior. And if you’re reducing risk, why not?”
* Hacking into and defending online black markets
  *  The operators of some big online black markets stayed online for a while despite a lot of determined efforts to break into them. 
  * [Darknet Diaries, episode 24: Operation Bayonet](https://darknetdiaries.com/episode/24/)
  * [Darknet Diaries, episode 58: Oxymonster](https://darknetdiaries.com/episode/24/)
* How the US military plans cyber-attacks.
  * [Darknet Darknet, episode 50: Operation Glowing Symphony](https://darknetdiaries.com/episode/50/)

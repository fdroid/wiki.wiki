Some FOSS libs for upstream dev.

## Barcode scanner

### Flutter

- https://github.com/mono0926/barcode_scan2
- https://github.com/RafaelBarbosatec/qr_code_dart_scan
- https://github.com/khoren93/flutter_zxing

### Native

- https://github.com/T8RIN/QuickieExtended

### React Native

- https://github.com/bugsdev2/rn-barcode-zxing-scan
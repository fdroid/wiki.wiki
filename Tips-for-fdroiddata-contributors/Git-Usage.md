When you contributing to fdroiddata, you need to interact with the repo via Git.
If you are not familiar with Git, please check following tips.

## Always use latest git version

Ancient Git versions may have weird default settings and bugs. Please always use recent version of Git.

## Prepare your local repo

First, you need to fork fdroiddata.
For that go to https://gitlab.com/fdroid/fdroiddata/-/forks/new and write your Gitlab username in "Select a namespace".
Do not change any other settings.

Then clone **the original repo**:

```
git clone --filter=blob:none https://gitlab.com/fdroid/fdroiddata.git
```

Now you should have a local repo.
In you local repo, add https://gitlab.com/<username>/fdroiddata as a remote repo.

```
git remote add <username> https://gitlab.com/<username>/fdroiddata.git
```

## Always create MRs from a clean branch

Now you can create a new branch:

```
git checkout -b <branch name> origin/master
```

After adding you changes, you can commit it

```
git add <changed file>
git commit -m <commit message>
```

Then push it to your fork and create a MR:

```
git push -u -o merge_request.create <username>
```

## Never open MR from master branch

As said above, you need to create a new branch from `origin/master`.
You can name it with whatever you want.
Do not open MRs from master branch because GitLab protect it by default so that reviewers can't edit your MR.

## Update your MR

When you need to update your MR, you can add changes then commit as usual. Then just push it to the remote branch and the MR is updated automatically. **Do not create new MRs.**

When you are working on your MR, origin branch is also updated.
Then you need to **rebase** your branch then force push it

```
git pull --rebase origin master
git push -f
```

Do not **merge** changes from origin branch. This usually causes conflicts.
When the MR is ready to merge, it needs to be rebased onto the origin branch.
If there is a conflict reviewers can't rebase it from the GitLab web UI.
Ancient Git versions merge origin changes by default so please always use the latest version and the `--rebase` option.

Sometimes your branch will have conflicts with the origin branch.
When you rebase the branch Git will ask you to solve those conflicts in your editor.
After the conflicts are solved, you can continue the rebase:

```
git rebase --continue
```

Sometimes it's easier to solve the conflicts automatically:

```
git pull --rebase origin master -Xours
```

but you need to check the result carefully.

F-Droid is built on signed index files. The signed index is a JAR file. The F-Droid Index Signature Scheme is a subset of [Java JAR Signature](https://docs.oracle.com/en/java/javase/21/docs/specs/man/jarsigner.html#the-signed-jar-file).  JAR Signatures were chosen as the base because every version of Android has built-in support for verifying them.  Also, it was the original "v1" signature scheme [for APKs](https://source.android.com/docs/security/features/apksigning/#v1).

All signed JARs used in _fdroidserver_ use the same format of what is included in the JAR:

* Data file: _entry.json_, _index-v1.json_, _index.xml_, or _publishsigkeys.json_
* Signature Block File: _META-INF/*.(DSA|EC|RSA)_
* Signature File: _META-INF/*.SF_
* JAR manifest: _META-INF/MANIFEST.MF_
* Self-signed Signer Certificate in the Signature Block File (e.g. no CA signatures), following the APKv1 model.

For example:

```console
 $ unzip -l tests/repo/entry.jar
Archive:  tests/repo/entry.jar
  Length      Date    Time    Name
---------  ---------- -----   ----
     2220  2024-02-21 14:54   entry.json
      206  2024-02-21 14:54   META-INF/SOVA.SF
     1913  2024-02-21 14:54   META-INF/SOVA.RSA
      107  2024-02-21 14:54   META-INF/MANIFEST.MF
---------                     -------
     4446                     4 files
```

The full JAR Signature spec is very complex, including all the crap from X.509 and ASN.1.  The above is all that is needed for index files.  [Here](https://gitlab.com/fdroid/fdroidserver/-/issues/1068#note_1884804260) is a plan to actually enforce those conditions.
#!/bin/bash -e

cd $(dirname $0)
for f in Internal/*.md; do
    name=$(basename "${f//-/ }")
    printf -- "- [${name%.md}](${f%.md})\n"
done \
    > Internal.md


# per app runs to speed up fdroid update and allow parallel builds

```
for every appid
  if metadata/appid.yml is older then repo_appid/index-v2.json:
    continue
  ln -s repo_appid repo
  fdroid build appid
  fdroid update
  rm repo
endfor

new_appids = []
for every appid
  if repo_appid/index-v2.json is newer then global_repo/index-v2.json:
    cp -r repo_appid/* global_repo/
    new_appids.append(repo_appid/index-v2.json)
endfor

merge_index(tmp/index-v2.json, new_appids)
sign(global_repo/index-v2.json)
```
One problem would be that we would ask for apk signing for every app but I think we can move that to the global step.

## questions

* Are the extracted icons included in the index-v2?
  * does not matter as we copy the complete local repo to the global one.
* For screenshots, what about if an older version gets built after the current version?  Doing a per-app update, I think the current logic in `fdroid update` would copy over the old screenshots over the current ones then.
  * the behavior would be the same as before.
# Processings

## websites
 * f-droid.org
 * fdroid.link
 * search.f-droid.org
 * monitor.f-droid.org
 * verification.f-droid.org
 * onion service
## DNS server?
## cloudflare.f-droid.org
## F-Droid Client App
## F-Droid Basic
## F-Droid Nearby
## crash report emails
## F-Droid Server (CLI)

## gitlab.com/fdroid
## opencollective.com/f-droid
## opencollective.com/f-droid-euro
## liberapay.com/F-Droid-Data
## github.com/sponsors/f-droid

## fdroid.net emails
## f-droid.org emails
## forum.f-droid.org
## matrix.f-droid.org
## irc?
## telegram?
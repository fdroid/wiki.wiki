## Upload the apk to GitLab release

- Create a token with api and write_registry scopes.
- Create a new package:

```
curl --fail-with-body --header "PRIVATE-TOKEN: <token>" \
     --upload-file ./path/to/F-Droid.apk \
     "https://gitlab.com/api/v4/projects/36189/packages/generic/org.fdroid.fdroid/<version>/org.fdroid.fdroid_<versionCode>.apk"
curl --fail-with-body --header "PRIVATE-TOKEN: <token>" \
     --upload-file ./path/to/F-Droid.apk.asc \
     "https://gitlab.com/api/v4/projects/36189/packages/generic/org.fdroid.fdroid/<version>/org.fdroid.fdroid_<versionCode>.apk.asc"
```

- Create a new release at https://gitlab.com/fdroid/fdroidclient/-/releases. Copy the release note from [changelog](https://gitlab.com/fdroid/fdroidclient/-/blob/master/CHANGELOG.md). Add the apk as a Package asset with the url from https://gitlab.com/fdroid/fdroidclient/-/packages/. The asc file is added as an Other asset instead.

## (optional) repo fallback download link

Check if the client download link set to the latest archived version of F-Droid Client. (We're using archived version there, because archived links will remain stable. i.e. repo links become outdated when archived.): https://gitlab.com/fdroid/fdroiddata/-/blob/master/repo/index.html
We aim to refactor the build stage and the cycle scheduling to be modular.  In the process, we will ditch as much custom code as possible.  There are many replacements available.  For example:

* We already use Vagrant, and it provides an abstraction layer for VMs, so we don't need our custom Python wrapper code.
* Docker/Podman/Vagrant/etc provide "push" and "pull" commands, so we don't need our custom Paramiko/SFTP code.

Also, for scheduling, we will modularize the functionality that is in `fdroid build` and then use that with in Buildbot to handle scheduling.  This is a place to work out the architecture and stages of implementation.  The whole process the _buildserver_ is quite different than the Buildbot workflow.  Buildbot is well developed and maintained, so it will save us a lot of work in the long run to make the switch.

The easiest place to get started with this is to read [Buildbot Concepts](https://docs.buildbot.net/current/manual/concepts.html).

### Production launch countdown

- [x] email notification
- [x] build logs gzip streamed to _unsigned/_
- [ ] generate status _build.json_
- [ ] deploy _build.json_ to https://f-droid.org/repo/status/build.json
- [ ] edit _control/build.sh_ to run `fdroid bbschedule` and stop sending emails
- [ ] _buildbot_ install method (Debian? pip? venv?)
- [ ] Run Master in its own user account
- [ ] systemd unit to start Master and Workers
- [ ] nail down Worker instance config
- [ ] automatic Worker auth config
- [ ] HTTP Auth on buildbot web on http://localhost
- [ ] integrate and merge all code
- [ ] Ansible set up for deploying Buildbot setup



### Mapping _fdroidserver_ to Buildbot

[![master.svg](uploads/13bc0c5798fe0e783434afc710b0b600/master.svg)](https://docs.buildbot.net/current/manual/introduction.html)

* `fdroid build --all` scheduling turned into standalone command as a _ChangeSource_ via `sendchange`
* Each _Change_ contains Application ID, Version Code, and size of _buildserver_ instance required (e.g. if `timeout:` is set, then use large _buildserver_ instance, otherwise use small.)
* There is a _Builder_ for each _buildserver_ instance size/type to handle the queue.
* There could be one _BuildFactory_ shared between the _Builders_ if it simplifies the code.
* There is one _Worker_ instance for each VM/bare metal instance connected to the service.

### Read-only website copy

Hopefully, we can make a public read-only copy of the web interface where the Buildbot SQLite data is pushed from the production _buildserver_ to a public read-only web instance of Buildbot.

* https://docs.buildbot.net/latest/manual/configuration/www.html#reverse-proxy-configuration


### Implementation Stages

1. ~~Get builds running in existing _buildserver_ VM by making a `Worker` per _buildserver_ instance that handles starting/stoping the VM, then running `fdroid build --server` for one single build.~~
2. ~~Make `fdroid build` just take the command line args and generate the `Changes` and submit them to the `Scheduler` via `buildbot sendchange`.~~
3. ~~Manage _buildserver_ VM start/reset from Buildbot workflow, each `Build` should run in a fresh VM instance.~~
4. ~~Support multiple _buildserver_ VM instances while still running `fdroid build`.~~
5. Support `fdroid update` running in parallel with `fdroid build`
6. Support `fdroid deploy` running in parallel with `fdroid build`
7. Support `fdroid publish` running in parallel with `fdroid build`

`checkupdates` can probably run as its own instance of Buildbot.

### Prototype Set up

* box running https://gitlab.com/fdroid/fdroid-bootstrap-buildserver/

```console
~$ git clone https://gitlab.com/eighthave/verification-buildbot buildbot.git
~$ cd buildbot
~/buildbot$ ./create-venv.sh
~/buildbot$ source venv/bin/activate
~/buildbot$ export auth=verify:pw # pw is in change_sources in master/master.cfg
~/buildbot$ buildbot checkconfig master && buildbot stop master && buildbot upgrade-master master && buildbot start master && buildbot-worker restart fdroid-worker
~/buildbot$ 
```
* `apt install fuse-overlayfs slirp4netns catatonit netavark buildah podman`
* Install _python3-podman_ from trixie: 
```console
$ wget https://deb.debian.org/debian/pool/main/p/python-podman/python3-podman_4.7.0-2_all.deb
$ apt install ./python3-podman_4.7.0-2_all.deb
```
* Run as the _fdroid_ user:
```bash
systemctl --user enable podman.socket
systemctl --user start podman.socket
systemctl --user status podman.socket
```
* `virt_container_type: podman` in _~/fdroiddata/config.yml_

### buildbot.fdroid.net

* Temporary bare metal prototyping box for mass rebuilds.

```console
$ ssh fdroid@buildbot.fdroid.net
256 SHA256:sRtaAFIkWZgyxbnN3k66o1dRb+L/BJySli1aErYpZQ4 (ECDSA)
256 SHA256:VJ+5O3LNlExKfIcDyEFeurJ8S2xTv+ctxaqObMzMNeQ (ED25519)
3072 SHA256:0KvFaWtk/FhsDt2VnFvuatVECXuAMguU0z+Atju+YG0 (RSA)
```


# build

The build _worker_ could use a [`LatentWorker`](https://docs.buildbot.net/latest/manual/configuration/workers.html#latent-workers), either [`LibvirtWorker`](https://docs.buildbot.net/latest/manual/configuration/workers-libvirt.html) or we implement a `VagrantLatentWorker`.  At this point, I think `VagrantLatentWorker` will work best.  This setup would be cleaner and match the docs, but could be tricky to actually setup.


# verification

pip3 install         'buildbot-console-view==2.10.1'         'buildbot-grid-view==2.10.1'         'buildbot-waterfall-view==2.10.1'         'buildbot-www==2.10.1' buildbot-worker==2.10.1
Collecting buildbot-console-view==2.10.1



# checkupdates

* The `fdroid checkupdates` runs are being implemented using a [`DockerLatentWorker`](https://docs.buildbot.net/3.5.0/manual/configuration/workers-docker.html).  

* Each active app should get a `ChangeSource` set up on its source repo.  Then when that detects new commits, it should trigger a `fdroid checkupdates` run for that app.

* There should be a new job that uses a Git watcher on fdroid/fdroiddata, then whenever there are new commits, it add/updates/deletes all the relevant `ChangeSources` for all the active apps.


# `fdroid build` redo break-down

## prepare build schedule

1. check repo and unsigned directories for existing builds
1. check fdroid data for all known builds and diff with existing builds

## _unclassified intermediate steps_

1. push local fdroidserver version to build VM/container
1. push build code and metadata to build VM/container
1. install NDK to build VM/container
1. run sudo provision in VM/container
1. run build in VM/container and capture output as log (aka *run-this-build*)
1. pull build result from VM/container
1. pull/write build log to _unsigned/_

## _(wip) commands for doing scheduling runs_

- run the scheduler and send the result as change notifications to buildbot
  ```
  export "auth=verify:ohl3acuD0looCiengob3abae3eiquaec"
  fdroid bbschedule | fdroid bbsend
  ```

- kick off an individual buildbot job:
  ```
  export "auth=verify:ohl3acuD0looCiengob3abae3eiquaec"
  fdroid bbsend org.fdroid.fdroid:1021051
  ```

## _(wip) individual build step commands_

(This is basically extracts what `master/master.cfg` is doing into a shell script, to make it test- and debug-able.)

1. `fdroid up org.fdroid.fdroid:1020050`
1. `fdroid build_inside_sudo org.fdroid.fdroid:1020050`
1. `fdroid fetchsrc org.fdroid.fdroid:1020050`  
1. `fdroid push org.fdroid.fdroid:1020050`
1. `fdroid vmprepare org.fdroid.fdroid:1020050`  
1. `fdroid exec org.fdroid.fdroid:1020050 -- ./fdroid scanner --verbose --refresh --exit-code`  
   (TODO: untangle, currently implemented in master/master.cfg, should be a separate subcommand)
1. `fdroid vmbuild org.fdroid.fdroid:1020050`  
   (TODO: untangle, currently implemented in master/master.cfg, should be a separate subcommand)  
1. `fdroid exec org.fdroid.fdroid:1020050 -- ./fdroid scanner --verbose --refresh --exit-code unsigned/org.fdroid.fdroid_1020050.apk`  
   (TODO: untangle, currently implemented in master/master.cfg, should be a separate subcommand, turning this into a separate subcommand will allow us to remove buildbot properties for package name and version code for some more complexity reduction/decoupling)
1. `fdroid pull org.fdroid.fdroid:1020050`
1. `ANDROID_HOME='/usr/lib/android-sdk' && fdroid verify --reuse-remote-apk --output-json --verbose unsigned/org.fdroid.fdroid_1020050.apk`
1. `fdroid destroy org.fdroid.fdroid:1020050`

related debug tooling:

* `fdroid vmshell org.fdroid.fdroid:1020050`
* `fdroid vmstatus org.fdroid.fdroid:1020050`


## post-`build` trigger

This is a hack to get Buildbot taking over from `fdroid build` (e.g. Implementation Stages 1-4).  It is a short term solution that lets us put Buildbot in place, then incrementally work on adding more stages to it.  At the end of _control/build.sh_, an email is sent to `PUBLISH`  to trigger the APK signing, e.g. `fdroid publish`.  This is the trigger that has to be sent after all the builds are done.

1. `fdroid bbschedule` sends Changes with a property `buildCycleSize` of the total number of package build in this batch.
2. _buildserver_ Builder has a new build step `BuildCycleCompleteNotification`
3. That checks if the global build countdown was set, if not, it sets the `buildCycleSize` property.
4. When `buildCycleSize` is 0, `BuildCycleCompleteNotification` will send the email.
5. When `buildCycleSize` is > 0, `BuildCycleCompleteNotification` decrements `buildCycleSize` property


## uniq's local dev environment

There's a vagrant based development environment for running all of this in a self-contained vagrant vm. Here's how to get that running:

```
git clone https://gitlab.com/uniqx/verification-buildbot.git
cd verification-buildbot
git checkout bbenv
vagrant up
tools/dbg-restart-buildbot.sh
```

now the development buildbot instance is available at: http://localhost:10010

to kick off a buildbot build run of fdroidclient, inside of buildbot you can run:

```
tools/dbg-send-build-fdroidclient.sh
```

Under the hood this installs verification buildbot into a vagrant VM. It also installs fdroidserver to /home/fdroid/fdroidserver and fdroiddata into /home/fdroid/fdroiddata inside the vagrant VM. To get a shell for debugging fdroid commands you can:

```
vagrant ssh -c 'sudo -iu fdroid'
cd fdroiddata
fdroid up org.fdroid.fdroid:1020051
```

## simple fdroid build (aka *run-this-build*)

The same functionality as the currently existing command for doing exactly on build locally without any VM/container: `fdroid build <package>:<ver>`.  The code in _fdroidserver/build.py_ does not encapsulate this in a single function or subcommand though, so the new command will have to rework the existing code.

requirements:
* code is assumed to be already in place (fdroiddata layout './build/packageName') and checked out to the correct version
* build metadata is in place (fdroiddata layout './metadata/packageName.yml') contains at least the `builds` block
* ignores sudo, srclibs (and extlibs) (assumes libs and dependencies are already in place)

related:
https://gitlab.com/fdroid/fdroidserver/-/issues/872
https://gitlab.com/fdroid/fdroidserver/-/merge_requests/1283

# Other buildbot instances

* https://flathub.org/builds/#/
* https://buildbot.python.org/all/#/
* https://build.webkit.org/#/
* https://github.com/MythTV/ansible
* OpenVPN uses libvirt with Buildbot:
  * https://github.com/OpenVPN/openvpn-buildbot
  * https://github.com/OpenVPN/openvpn-vagrant

# mass builds

## mass builds for consolidating fdroid server build code

For doing builds in buildbot we've created a refactored version `fdroidserver/build.py` that doesn't include the `--server` feature. For making sure this refactored version is capable of building all we're doing mass builds to find issues and fix them.

### outdated apps builds

A lot of the apps in fdroiddata are outdated to a point where the current production version of `fdroid build --server` can't build them anymore. Naturally hose builds won't be supported by the new build script either. Here's a list of errors that occur due to outdated apps:

* build_inside_sudo:
  * `E: The value 'stretch-backports' is invalid for APT::Default-Release as such a release is not available in the sources` tired setup outdated software repo
  * `E: Package 'openjdk-11-jdk-headless' has no installation candidate` tired to install outdated java version
  * `sysctl: permission denied on key "fs.inotify.max_user_watches"` only a problem in podman
* fetchsrc:
  * `fdroidserver.exception.VCSException: Invalid vcs type`
  * `error: pathspec '421e0212c' did not match any file(s) known to git` release revision got deleted by upstream
  * `fdroidserver.exception.VCSException: Git clone failed` source code not available
* build_local_prepare:
  * `fdroidserver.exception.BuildException: Failed to apply patch metadata/...` build holds a patch file, that's deleted by upstream
* build_local_run:
  * `Could not determine java version from '17.0.13'.` tired to use outdated java version
  * `General error during semantic analysis: Unsupported class file major version 61` tried to use ouddated gradel version
  * `bash: line 1: python: command not found` depends on python2
  * `could not resolve all files for configuration ...` broken gradle dependencies
  * `java.lang.NoClassDefFoundError: Could not initialize class org.codehaus.groovy.vmplugin.v7.Java7` oudated gradle version
  * `Unrecognized VM option 'MaxPermSize=2048m' java17 doesn't support that option flag anymore`
  * `Unable to make field private int java.io.FileDescriptor.fd accessible: module java.base does not "opens java.io" to unnamed module @24273305`
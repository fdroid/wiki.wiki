1. Download files from https://f-droid.org/F-Droid.apk and https://f-droid.org/F-Droid.apk.asc
2. Create a package in https://gitlab.com/fdroid/fdroidclient/-/packages/

```
curl --location --header "PRIVATE-TOKEN: <Token>" --upload-file F-Droid.apk "https://gitlab.com/api/v4/projects/36189/packages/generic/org.fdroid.fdroid/<versionName>/org.fdroid.fdroid_<versionCode>.apk"
curl --location --header "PRIVATE-TOKEN: <Token>" --upload-file F-Droid.apk.asc "https://gitlab.com/api/v4/projects/36189/packages/generic/org.fdroid.fdr
oid/<versionName>/org.fdroid.fdroid_<versionCode>.apk.asc"
```

3. Create a release in https://gitlab.com/fdroid/fdroidclient/-/releases/new.
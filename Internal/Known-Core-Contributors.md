This is a ever-changing list of known core contributors, where they can list their capabilities and whether they want to be pinged or not.  There are a couple places to see who has what access:

* [@fdroid "group" on gitlab.com](https://gitlab.com/groups/fdroid/-/group_members)
* [f-droid "project" on Weblate](https://hosted.weblate.org/access/f-droid/#users)
* [@contributors "group" on forum.f-droid.org](https://forum.f-droid.org/g/contributors)
* [Contributors listing on f-droid.org](https://f-droid.org/en/about/#contributors)

Please consider adding yourself to this list (in alphabetic order), to give the other team members and community contributors some hints.

| Name | About me / tasks at F-Droid | Pinging via @ |
| ------ | ------ | ------ |
| @bene64 | I'm a new contributor, currently primarily helping out at [Data](https://gitlab.com/fdroid/fdroiddata). Generally I have some experience with GitLab CI (but not for the F-Droid scripts). | You can ping me, but for most tasks, I'm probably not the one who can help a lot. At the moment it can take some days until I react.|
| @eighthave | I mainly work on _fdroidserver_, _issuebot_, infrastructure, and _fdroidclient_ | I prefer to be pinged in GitLab issues and merge requests |
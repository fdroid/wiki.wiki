Right now, the ACRA crash report [handling](https://gitlab.com/fdroid/acra-crash-reports/-/blob/master/parse-stacktrace-json-to-gitlab.py) is super kludgey and manual.  It can be fully automated without too much effort.  Here is my general idea for how to do that:

- [x] Attach crash reports as an easily parsable format (e.g. _ACRA-report.stacktrace.json_) 
- [ ] Make a dedicated POP3 mailbox for the crash reports to be received.
- [ ] Make script fetch messages from POP3, deleting them from the server as they are fetched.
- [ ] _parse-stacktrace-json-to-gitlab.py_ to post to GitLab issues from each email
- [ ] Generate metrics from emails, in the fdroid/metrics style.
- [ ] Keep local cache of emails for four weeks.
- [ ] Review history to see if there are any privacy leaks.
- [ ] New crash reports are set to Confidential.
- [ ] Crash report maintainers can make issues public, if they have no private info.
- [ ] Make https://gitlab.com/fdroid/acra-crash-reports a publicly visible project.

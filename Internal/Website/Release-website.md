Here is the official [Release Process](../Release-Process#fdroid-website).

Here's @uniqx cheat sheet for making a new release tag:

```
cd .../fdroid-website
git fetch fdroid --tags --prune
git checkout master
git reset fdroid/master --hard
tools/make-release-tag.sh
git push fdroid GIT-TAG
```

You'll have to replace `GIT-TAG` in the last command with whatever git tag number was created when running `tools/make-release-tag.sh`.

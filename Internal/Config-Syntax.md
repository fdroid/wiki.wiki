The _fdroidserver_ tools have an extensive syntax for configuration to provide a
human-readable format to provide simple yet flexible configuration.  The syntax
is fundamentally a subset of [YAML 1.2](https://yaml.org/spec/1.2.2/), with
only specific data structures supported.  The configuration parsing also convert
some data formats to allow for simpler forms of basic configuration.

At the base level, a config file is a collection of key/value mapping (aka a
"dict").  The keys are the config options, and the values are the data used to
set things up.  More elaborate config items are available in three formats:

* strings
* sequences of strings (aka list of strings)
* sequences of mappings (aka list of dicts)

For example, the `mirrors:` config item can be one entry or many that include metadata:

```yaml

mirrors: https://theone.true.mirror/

mirrors:
  - https://first.mirror
  - https://second.mirror

mirrors:
  - url: https://usa.mirror
    countryCode: US
  - url: https://kenya.mirror
    countryCode: KN
```


## Localization

There are additional structures set up to allow for localization of the config
values, like Anti-Features or Categories.  This format is designed to work
nicely with translation tools like Weblate.


The _fdroidserver_ tools are the essential tool for managing repositories of Android APK files.  All valid APKs are [signed](https://source.android.com/docs/security/features/apksigning), but Android only verifies the identity of the signer for updates, not first time installs.  For a long time, _fdroidserver_ has been extracting the signer (the X.509 Signing Certificate) from the APK to be able to identify and track signers.  This information was first used by the client app to check whether an APK was signed with a compatible key with what was installed without having to download the APK first.

Over time, it has become clear that it is useful to check the signer for other reasons, and now more tools are checking the signer in addition to the signature itself.

* Online APK scanner websites like Pithus add signer fingerprints TODO link
* [`AllowedAPKSigningKeys:`](https://f-droid.org/docs/Build_Metadata_Reference/#AllowedAPKSigningKeys) was added to the metadata.
* `fdroid install` to verify that an APK is signed by the right key.

But it turns out the really old certificate extraction method was not fully up to the task of strictly verifying APKs.  The first piece of the issue was first discovered and [reported](https://gitlab.com/fdroid/fdroidserver/-/issues/1128) by Jason Donenfeld.  `get_first_signer_certificate()` checked the APK signature versions in the reverse order that `apksigner verify` does. `fdroid update` already ran `apksigner verify` on all APKs before publishing them, and `apksigner` includes a [check](https://android.googlesource.com/platform/tools/apksig/+/refs/tags/android-13.0.0_r3/src/main/java/com/android/apksig/ApkVerifier.java#383) that all the included signer certificates match.  So this restricted the exploitability quite a bit.  Then @obfusk [exposed](https://github.com/obfusk/fdroid-fakesigner-poc/) one key detail that turned an interesting bug into a possible exploit vector.  When the minimum supported Android version (`minSdkVersion`) is high enough (TODO which version? I think 30?), then Android's signature verification does not read the old JAR signature certificate at all. So this meant that `fdroid` and `apksigner` would see different signers:

```console
$ apksigner verify --print-certs fdroidserver-issue-1128-poc_jochensp_F-Droid.apk | grep 256
DOES NOT VERIFY
ERROR: Missing META-INF/MANIFEST.MF
$ apksigner verify --print-certs --min-sdk-version 24 fdroidserver-issue-1128-poc_jochensp_F-Droid.apk | grep 256
Signer #1 certificate SHA-256 digest: 09350d5f3460a8a0ea5cf6b68ccd296a58754f7e683ba6aa08c19be8353504f3
$ python3 -c "from fdroidserver import common; \
    print(common.apk_signer_fingerprint('fdroidserver-issue-1128-poc_jochensp_F-Droid.apk'))"
43238d512c1e5eb2d6569f4a3afbf5523418b82e0a3ed1552770abb9a9c9ccab
```

This highlights one hazard of using multiple implementations of verification: different implementations can open up exploit vectors. The Master Key and Janus vulnerabilities are two examples of that in the Android OS itself.  The official verification tool [`apksigner`](https://developer.android.com/tools/apksigner) does not provide a publicly documented API to extract the signing certificates.  [Starting](https://android.googlesource.com/platform/tools/apksig/+/656c7e0acb26c512537c0907cbdf4fdfe16c31cf) in _build-tools_ v33.0.0, there is `apksigner verify --print-certs-pem`, but it is unclear whether that is intended as a private or public API. Even if it is private, it does provide a useful tool for testing a Python implementation.

## further notes

* The ancient, deprecated, custom F-Droid "sig" algorithm still needs to be supported, that means we need certificate extraction.
* Using private APIs is hazardous
* There is a [public API](https://android.googlesource.com/platform/tools/apksig/+/refs/tags/android-11.0.0_r31/README.md) in the Java `apksig` library, but _fdroidserver_ is written in Python.
* The `--print-certs` and `--print-certs-pem` options in `apksigner verify` do provide us a reliable way to test our Python implementation provides the same output as the canonical Java implementation.
* `--print-certs-pem` still needs to be converted from PEM to DER to get the SHA-256 fingerprint
* There can be bugs in our scraper as well, given that _apksigner_ can dump all sorts of warnings, and does not seem to have been designed to have its output scraped, unlike tools like _aapt_.  The _apksig_ Java library seems to be the official API.

* https://social.librem.one/web/statuses/112226063399132142
* https://github.com/obfusk/fdroid-fakesigner-poc/
* https://www.openwall.com/lists/oss-security/2024/04/08/8

To be affected in f-droid.org, the APK must:

* Use `Binaries:`
* Use `AllowedAPKSigningKeys`
* Have a minSdkVersion of 24 or higher

To exploit, they would have to get the APK into the server that `Binaries:` URL points to,
then make it build reproducibly.

For repos made with collections of binary APKs, this more of a concern. `AllowedAPKSigningKeys` was designed with these kinds of repos in mind.  Our goal is to make it as simple as possible to safely run app repos.  `AllowedAPKSigningKeys` is a feature along those lines.  So this page is about documenting what needs to happen to make `AllowedAPKSigningKeys` as reliable as possible.  This issue doesn't really affect f-droid.org, It does affect repos like IzzySoft if they do not provide other methods of ensuring that the APKs added to the repo can be trusted to be what they claim to be.

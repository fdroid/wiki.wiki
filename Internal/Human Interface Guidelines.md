
# F-Droid Human Interface Guidelines

This is a sketchpad for developing a consistent and documented HIG for F-Droid.  It is mostly about the Android app _fdroidclient_, but UX should be consistent across the ecosystem.

## _fdroidclient_

These are some rules as of v 1.19.1

* All primary navigation happens with the bottom nav.
* The sidebar is never used because it conflicts with other side-swiping gestures.
* When all the information does not fit on a single screen, vertical scrolling is used to view more information.
* When a screen has a lot of information and it becomes too confusing to scroll, it can include collapsable sections to hide the information that is rarely used, or most likely to be confusing.
* Tabs are not used in any part of the interface. 
* Long-press is not used in the interface.


## Examples from other ecosystems

* https://developer.gnome.org/hig/
* https://developer.apple.com/design/human-interface-guidelines/designing-for-ios

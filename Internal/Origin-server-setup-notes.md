- started out with fresh bare metal server from hetzner.com, with 2 extra disks
- boot server into hetzner recovery, and connect via ssh
- run `installimage`, hetzners utility script for installing an operating system
- pick debian 12
- comment out DEVICE /dev/sda and DEVICE/dev/sdb (hdds)
- set raid mode to 1 instead of 6
- close editor and execute configuration by pressing F10
 
- wait for new install to boot and connect via ssh
- installed some required software

```
apt install parted apache2
```

- took a look at disks/partitions with `lsblk`
- partition `/dev/sda`

```
parted /dev/sda
>(parted)> mktable
>(parted: new disk label type?)> gpt
>(parted)> mkpart
>(parted: Partition name?)> fdroid
>(parted: File system type?)> ext4 
>(parted: Start?)> 0%
>(parted: End?)> 100%
>(parted)> quit
```

- repeat partitioning for /dev/sdb

- setup raid

```
mdadm --create --level=1 --raid-devices=2 --homehost='<none>' /dev/md/3 /dev/sda1 /dev/sdb1
```

- checked out new raid array config:

```
mdadm --detail --scan
```
- updated `/etc/mdadm/mdadm.conf` accordingly. eg. I had to append following line:

```
ARRAY /dev/md/3  metadata=1.2 UUID=8b641bf9:2935abde:f2a202d3:8a144673 name=rescue:3
```

- make sure it gets loaded on next boot

```
update-initramfs -u -k all
```

- format the new raid disk and mount it

```
mkfs.ext4 /dev/md3
echo '/dev/md3 /var/www ext4 defaults 0 0' >> /etc/fstab
systemctl daemon-reload  # re-mount fstab, systemd style
```

- just checking if everything looks good

```
lsblk
```

- then copied files from a mirror, also see: https://f-droid.org/docs/Running_a_Mirror/

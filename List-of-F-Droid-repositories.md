# This Page Was Moved
This list is now maintained [in the F-Droid forum.](https://forum.f-droid.org/t/known-repositories/721)

Create an account there to contribute.

Note the [list of abandoned repositories](https://forum.f-droid.org/t/abandoned-repositories/10555).

F-Droid relies heavily on GitLab CI, and that means we need Runners.  We use the gitlab.com shared runners, as well as a number that are maintained by other organizations.  These runners need to be oriented towards stability.  It is easy to cause issues that cause jobs to fail mysteriously.  If the dev doesn't know it was caused by runner instability, it can easily cause someone to spend hours trying to track down a problem.


## Shared GitLab Runners on Azure

When there is time available donated by Azure, then there can be
[Shared GitLab Runners on Azure](Continuous-Integration-(CI)/Shared-GitLab-Runners-on-Azure).  This is maintained by @proletarius101.


## OSUOSL runners

OSUOSL maintains some runners that are shared across a number of projects.  Maintained by [OSUOSL](https://osuosl.org/contact/): support@osuosl.org 

* [ci-aarch64-runner1-osuosl-docker-fdroid](https://gitlab.com/groups/fdroid/-/runners/15012154)
* [ci-x64-runner1-osuosl-docker-fdroid](https://gitlab.com/groups/fdroid/-/runners/15012034)
* [ci-x64-runner2-osuosl-docker-fdroid](https://gitlab.com/groups/fdroid/-/runners/15012150)

https://osuosl.org/services/hosting/details/#ci-cd-hardware-resources


## `pep-runner`

**Will be removed soon, the host is shutting down**

`quay.io/gitlab/gitlab-runner-docker-cleanup` is always running on this runner to clean up Docker cruft.

https://gitlab.com/groups/fdroid/-/runners/534721#/

@jspricke maintains this instance, which is donated by pep.security.  It is currently devoted to running _checkupdates_.

* 4 CPU
* 8GB RAM
* 100GB disk


## `fdroid-gitlab-runner-osuosl-01`

[`/usr/share/gitlab-runner/clear-docker-cache`](https://docs.gitlab.com/runner/executors/docker.html#clear-the-docker-cache) is always running on this runner as a weekly cronjob to clean up Docker cruft.

https://gitlab.com/groups/fdroid/-/runners/535145

@pmmayero manages this instance in a VM provided in the OSUOSL cloud.

* 8 CPU
* 16GB RAM
* 80GB disk
* KVM shared to containers

## `fdroid-gitlab-runner-osuosl-02`

[`/usr/share/gitlab-runner/clear-docker-cache`](https://docs.gitlab.com/runner/executors/docker.html#clear-the-docker-cache) is always running on this runner as a weekly cronjob to clean up Docker cruft.

https://gitlab.com/groups/fdroid/-/runners/43196296

@pmmayero manages this instance in a VM provided in the OSUOSL cloud.

* 8 CPU
* 16GB RAM
* 80GB disk
* KVM shared to containers

Please note that for the gitlab runners to successfully run the android emulator on docker, the `priviledged` key on the `/etc/gitlab-runner/config.toml` should be set to `true`

Below is a sample of the runner config.toml on `fdroid-gitlab-runner-osuosl-02`
```ini
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "fdroid-gitlab-runner-osuosl-02"
  url = "https://gitlab.com"
  id = 123654789654
  token = "insert-gitlab-token-here"
  token_obtained_at = 
  token_expires_at = 
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = true
    image = "docker:dind"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
```